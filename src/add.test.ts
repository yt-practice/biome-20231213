import { add } from './add'

describe('add', () => {
  const list: {
    title?: string
    args: Parameters<typeof add>
    ret: ReturnType<typeof add>
  }[] = [
    { args: [1, 1], ret: 2 },
    { args: [1, 2], ret: 3 },
  ]
  for (const { title, args, ret } of list) {
    it(title || JSON.stringify(args), () => {
      expect(add(...args)).toEqual(ret)
    })
  }
})
